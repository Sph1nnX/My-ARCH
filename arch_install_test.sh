pacman -Syy nano vim git go wget curl reflector

#cryptsetup luksFormat --perf-no_read_workqueue --perf-no_write_workqueue --type luks2 --cipher aes-xts-plain64 --key-size 512 --iter-time 8000 --pbkdf argon2id --hash sha3-512 /dev/sda2
#cryptsetup --allow-discards --perf-no_read_workqueue --perf-no_write_workqueue --persistent open /dev/sda2 r00t

mount /dev/sda2 /mnt

btrfs sub create /mnt/@ && \
btrfs sub create /mnt/@home && \
btrfs sub create /mnt/@abs && \
btrfs sub create /mnt/@tmp && \
btrfs sub create /mnt/@srv && \
btrfs sub create /mnt/@snapshots && \
btrfs sub create /mnt/@log && \
btrfs sub create /mnt/@cache && \
btrfs sub create /mnt/@swap

umount /mnt

mount -o noatime,commit=120,space_cache=v2,ssd,autodefrag,subvol=@ /dev/sda2 /mnt && \
mkdir -p /mnt/{boot/,home,var/cache,var/log,.swap,.snapshots,var/tmp,var/abs,srv} && \
mount -o noatime,commit=120,space_cache=v2,ssd,autodefrag,subvol=@home /dev/sda2 /mnt/home  && \
mount -o nodev,nosuid,noexec,noatime,commit=120,space_cache=v2,ssd,autodefrag,subvol=@abs /dev/sda2 /mnt/var/abs && \
mount -o nodev,nosuid,noatime,commit=120,space_cache=v2,ssd,autodefrag,subvol=@tmp /dev/sda2 /mnt/var/tmp && \
mount -o noatime,commit=120,space_cache=v2,ssd,autodefrag,subvol=@srv /dev/sda2 /mnt/srv && \
mount -o nodev,nosuid,noexec,noatime,commit=120,space_cache=v2,ssd,autodefrag,subvol=@log /dev/sda2 /mnt/var/log && \
mount -o nodev,nosuid,noexec,noatime,commit=120,space_cache=v2,ssd,autodefrag,subvol=@cache /dev/sda2 /mnt/var/cache && \
mount -o noatime,commit=120,space_cache=v2,ssd,autodefrag,subvol=@snapshots /dev/sda2 /mnt/.snapshots && \
mount -o noatime,commit=120,space_cache=v2,ssd,autodefrag,subvol=@swap /dev/sda2 /mnt/.swap && \
mount -o nodev,nosuid,noexec /dev/sda1 /mnt/boot/


truncate -s 0 /mnt/.swap/swapfile && \
chattr +C /mnt/.swap/swapfile && \
fallocate -l 32G /mnt/.swap/swapfile && \
chmod 600 /mnt/.swap/swapfile && \
mkswap /mnt/.swap/swapfile && \
swapon /mnt/.swap/swapfile

#pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com
#pacman-key --lsign-key FBA220DFC880C036
#pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/#chaotic-mirrorlist.pkg.tar.zst'
#echo '[chaotic-aur]
#Include = /etc/pacman.d/chaotic-mirrorlist' >> /etc/pacman.conf

nano /etc/pacman.conf

pacstrap /mnt base base-devel linux-zen linux-zen-headers zsh zsh-completions nano vim bash bash-completion linux-firmware intel-ucode btrfs-progs zstd

cp /etc/pacman.conf /mnt/etc/pacman.conf

genfstab -U /mnt >> /mnt/etc/fstab

timedatectl set-ntp true

cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist

arch-chroot /mnt sh -c "$(curl -fsSL https://codeberg.org/Sph1nnX/My-ARCH/raw/branch/main/arch_install_ch_test.sh)"

