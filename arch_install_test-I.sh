#!/bin/bash
import os
import subprocess
import getpass

def main():
    username = input("Bitte geben Sie den gewünschten Benutzernamen ein: ")
    password = getpass.getpass("Bitte geben Sie das gewünschte Passwort ein: ")

    all_disks = list(filter(lambda d: d.startswith("/dev/sd"), os.listdir("/dev")))

    for i, disk in enumerate(all_disks):
        print(f"{i}: {disk}")

    disk_index = int(input("Wählen Sie die Festplatte, auf der Arch Linux installiert werden soll (Geben Sie die Zahl ein): "))
    selected_disk = all_disks[disk_index]

    # Lassen Sie den Benutzer die Partitionierung manuell durchführen
    print("Bitte führen Sie die Partitionierung manuell durch.")
    os.system(f"parted {selected_disk}")

    # Den Benutzer bitten, die neu erstellten Partitionen einzugeben
    efi_partition = input("Geben Sie die EFI-Partition ein (z. B. /dev/sda1): ")
    boot_partition = input("Geben Sie die Boot-Partition ein (z. B. /dev/sda2): ")
    root_partition = input("Geben Sie die Root-Partition ein (z. B. /dev/sda3): ")

    # Benutzer nach dem gewünschten Dateisystem fragen
    filesystem_choice = input("Wählen Sie das Dateisystem (btrfs oder ext4): ").lower()

    while filesystem_choice not in ("btrfs", "ext4"):
        print("Ungültige Auswahl, bitte erneut eingeben.")
        filesystem_choice = input("Wählen Sie das Dateisystem (btrfs oder ext4): ").lower()

    # Partitionen formatieren und benennen
    subprocess.run(["mkfs.fat", "-F32", efi_partition, "-n", "EFI"], check=True)
    subprocess.run(["mkfs.fat", "-F32", boot_partition, "-n", "BOOT"], check=True)
    
    # LUKS-Verschlüsselung
    use_luks = input("Möchten Sie die Root-Partition mit LUKS verschlüsseln? (y/n): ").lower()
    while use_luks not in ('y', 'n'):
        print("Ungültige Auswahl, bitte erneut eingeben.")
        use_luks = input("Möchten Sie die Root-Partition mit LUKS verschlüsseln? (y/n): ").lower()

    if use_luks == 'y':
        luks_passphrase = getpass.getpass("Geben Sie das gewünschte LUKS-Passwort ein: ")

        subprocess.run([
            "cryptsetup", "luksFormat", "--perf-no_read_workqueue",
            "--perf-no_write_workqueue", "--type", "luks2", "--cipher",
            "aes-xts-plain64", "--key-size", "512", "--iter-time", "8000",
            "--pbkdf", "argon2id", "--hash", "sha3-512", root_partition
        ], input=luks_passphrase.encode(), check=True)

        subprocess.run(["cryptsetup", "open", root_partition, "luks_root"], input=luks_passphrase.encode(),
                       check=True)

    cryptroot_path = f"/dev/mapper/luks_root" if use_luks == "y" else root_partition
    # Format the root partition
    subprocess.run(["mkfs." + filesystem_choice, cryptroot_path], check=True)

    # Mount the partitions
    subprocess.run(["mount", cryptroot_path, "/mnt"], check=True)
    subprocess.run(["mkdir", "-p", "/mnt/boot"], check=True)
    subprocess.run(["mount", boot_partition, "/mnt/boot"], check=True)
    subprocess.run(["mkdir", "-p", "/mnt/boot/efi"], check=True)
    subprocess.run(["mount", efi_partition, "/mnt/boot/efi"], check=True)

    # Install base system
    subprocess.run(["pacstrap", "/mnt", "base", "base-devel", "dracut", "grub", "efibootmgr", "os-prober"], check=True)
# Generate fstab
    subprocess.run(["genfstab", "-U", "/mnt", ">>", "/mnt/etc/fstab"], check=True)

    # Enter the chroot
    os.system("arch-chroot /mnt")

    # Configure GRUB
    subprocess.run(["grub-install", "--target=x86_64-efi", "--efi-directory=/boot/efi", "--bootloader-id=GRUB"], check=True)
    subprocess.run(["grub-mkconfig", "-o", "/boot/grub/grub.cfg"], check=True)

    # Create a user and set their password
    subprocess.run(["useradd", "-m", username], check=True)
    subprocess.run(["passwd", username], input=(password + "\n" + password + "\n").encode(), check=True)

    # Download dotfiles from git
    clone_dotfiles = input("Möchten Sie Ihre Dotfiles von Git herunterladen? (y/n): ").lower()
    if clone_dotfiles == 'y':
        dotfiles_repo = input("Geben Sie die Git-Repository-URL für Ihre Dotfiles ein: ")
        subprocess.run(["git", "clone", dotfiles_repo, f"/home/{username}/.dotfiles"], check=True)

    # Exit the chroot
    os.system("exit")

    # Unmount the partitions
    subprocess.run(["umount", "-R", "/mnt"], check=True)

    print("Die Arch Linux-Installation ist abgeschlossen. Starten Sie das System neu, um Arch Linux zu verwenden.")

if __name__ == "__main__":
    main()