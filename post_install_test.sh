#!/bin/bash

paru -Syy mangohud mangohud-common lib32-mangohud vkbasalt replay-sorcery && \
paru -Syy wine-tkg-staging-fsync-git vkd3d-proton-tkg-git protontricks-git lib32-vkd3d-proton-tkg-git \
dxvk-mingw-git wine-mono wine-gecko winetricks-git proton-ge-custom-bin plasma5-applets-audio-device-switcher \
plasma5-applets-window-title-git plasma5-applets-redshift-control-git plasma5-applets-journalviewer-git \
plasma5-applets-fanspeed-monitor-git plasma5-applets-kde-arch-update-notifier-git easyeffects pavucontrol \
paprefs pasystray lutris steam steam-acolyte steam-native-runtime chromium firefox firefox-i18n-de \
libreoffice-fresh libreoffice-fresh-de hunspell-de opera opera-ffmpeg-codecs mpv-full-git youtube-dl \
google-earth-pro dbeaver dbeaver-plugin-office dbeaver-plugin-svg-format dbeaver-plugin-batik wps-office \
ttf-wps-fonts wps-office-mime atom calibre conky-manager2-git calligra corectrl helvum keepassxc kvantum-qt5 \
piper smb4k kmymoney skrooge latte-dock nextcloud-client noisetorch warpinator ktnef git gitkraken fluid \
flameshot blender gimp freecad inkscape kolourpaint digikam librecad synfigstudio krita krita-plugin-gmic \
mypaint displaycal luminancehdr hugin rawtherapee darktable perl pencil2d clipgrab converseen handbrake \
shotcut discord element-desktop mumble neochat nitroshare qbittorrent remmina virt-viewer virt-manager \
qemu-desktop signal-desktop teamspeak3 teamspeak3-addon-installer teamspeak3-kde-link-fix-hack \
teamspeak3-kde-wrapper teamviewer fractal filezilla xdman wire-desktop wireshark-qt thunderbird \
thunderbird-i18n-de qtwebflix-git zoom audacious audacity shortwave bitwig-studio lmms strawberry \
obs-studio obs-linuxbrowser-bin mixxx goverlay-git slack gvfs-smb hddtemp fd kscreen gputest wget \
fastfetch-git gamemode lib32-gamemode wqy-zenhei ttf-tw ttf-hannom libvirt wl-clipboard xclip easyeffects \
lsp-plugins zam-plugins mda.lv2 vst-host lv2-host lsp-plugins-docs ladspa-host ananicy-cpp-git ananicy-rules-git && \


sudo hwclock -uw && \
sudo timedatectl set-local-rtc 1 --adjust-system-clock && \

git clone https://codeberg.org/Sph1nnX/My-ARCH.git && \
cd My-ARCH/.config && \
cp -r * ~/.config/ && \


sudo umount /.snapshots && \
sudo rm -r /.snapshots && \
sudo snapper -c root create-config / && \
sudo mount -a && \
sudo chmod 750 -R /.snapshots && \


echo 'NoDisplay=true' | sudo tee -a /usr/share/applications/in.lsp_plug.lsp_plugins_* && \


sudo touch /etc/polkit-1/rules.d/90-corectrl.rules && \
echo 'polkit.addRule(function(action, subject) {
    if ((action.id == "org.corectrl.helper.init" ||
         action.id == "org.corectrl.helperkiller.init") &&
        subject.local == true &&
        subject.active == true &&
        subject.isInGroup("users")) {
            return polkit.Result.YES;
    }
});' | sudo tee -a /etc/polkit-1/rules.d/90-corectrl.rules && \



export PATH=/usr/bin/ && yay -S nerd-fonts-jetbrains-mono 

